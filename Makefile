CC=g++
CFLAGS=-W -Wall
GTKCFLAGS=`pkg-config --cflags gtk+-3.0`
GTKLIBS=`pkg-config --libs gtk+-3.0`

vulkan_utils_interface.o:
	$(CC) $(CFLAGS) $(GTKCFLAGS) -c vulkan_utils_interface.cpp
main.o:
	$(CC) $(CFLAGS) $(GTKCFLAGS) -c main.c

main: vulkan_utils_interface.o main.o
	$(CC) $(CFLAGS) $^ -o main $(GTKLIBS)

clean:
	rm -f main *.o

all: clean main

