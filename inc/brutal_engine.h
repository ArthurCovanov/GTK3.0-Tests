/*********************************/
/* Name        : brutal_engine.h */
/* Author      : Arthur Covanov  */
/* Version     : 0.1             */
/* Copyright   : GPL             */
/* Description : Br�tal Engine   */
/*********************************/

#ifndef INC_BRUTAL_ENGINE_H_
#define INC_BRUTAL_ENGINE_H_

#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>
#include <vector>

#define BRUTAL_ENGINE_MAKE_VERSION(major, minor, patch) \
    (((major) << 22) | ((minor) << 12) | (patch))
// Vulkan 1.0 version number

#define BRUTAL_ENGINE_VERSION_2_0 BRUTAL_ENGINE_MAKE_VERSION(2, 0, 0)// Patch version should always be set to 0

#define BRUTAL_ENGINE_MAJOR(version) ((uint32_t)(version) >> 22)
#define BRUTAL_ENGINE_MINOR(version) (((uint32_t)(version) >> 12) & 0x3ff)
#define BRUTAL_ENGINE_PATCH(version) ((uint32_t)(version) & 0xfff)
// Version of this file
#define BRUTAL_ENGINE_HEADER_VERSION 0

#define BRUTAL_ENGINE_SUCCESS 0
#define BRUTAL_ENGINE_FAILURE -1

typedef struct threadsDatas
{
		bool *runThread;
		bool *pause;
} threadsDatas;

class BrutalEngine
{
	public:
		BrutalEngine(); // Disallow creating an instance of this object
	protected:
	private:
		// initialize the VkApplicationInfo structure
		VkApplicationInfo initVkApplicationInfo();
		// initialize the VkInstanceCreateInfo structure
		VkInstanceCreateInfo initVkInstanceCreateInfo();
		VkInstanceCreateInfo initVkInstanceCreateInfo(const VkApplicationInfo *applicationInfo);
		// initialize the VkDeviceQueueCreateInfo structure
		void initVkDeviceQueueCreateInfo(VkDeviceQueueCreateInfo *deviceQueueCreateInfo);
		// initialize the VkDeviceCreateInfo structure
		void initVkDeviceCreateInfo(VkDeviceCreateInfo *deviceCreateInfo, VkDeviceQueueCreateInfo *deviceQueueCreateInfo);

		const VkApplicationInfo vkApplicationInfo = initVkApplicationInfo();
		// initialize the VkInstanceCreateInfo structure
		const VkInstanceCreateInfo vkInstanceCreateInfo = initVkInstanceCreateInfo(&vkApplicationInfo);

#ifdef NDEBUG
		//const std::vector<const char*> requiredLayersNames = {	"VK_LAYER_LUNARG_standard_validation"};
#else
		//static const std::vector<const char*> requiredLayersNames = { };
#endif

		//static const std::vector<const char*> requiredExtensionsNames = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };

		//static World *world;	//Game Engine's World singleton
		//static bool initialized;//Boolean to know if Br�tal Engine has been initialized
		/************************************/
		/* mutexes[0] : Upload Thread mutex */
		/* mutexes[1] : [...] mutex         */
		/************************************/
		static threadsDatas *updateThreadDatas;
		void beginUpdateThread();
		void endUpdateThread();
};

#endif /* INC_BRUTAL_ENGINE_H_ */
