//============================================================================
// Name        : vulkan_utils_interface.h
// Author      : Arthur Covanov
// Version     :
// Copyright   :
// Description :
//============================================================================

#ifndef _VULKAN_UTILS_INTERFACE_H_
#define _VULKAN_UTILS_INTERFACE_H_

#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>

int launchInterface(int argc, char *argv[]);

class VulkanUtilsInterfaceMainWindow
{
	public:
		VulkanUtilsInterfaceMainWindow(GtkBuilder *builder);
		~VulkanUtilsInterfaceMainWindow();
		void addTstTxt();
		void start();
	protected:
	private:
		GtkBuilder *builder;
		const char builderUi[11];
		GObject *mainWindow;
		GObject *launchButton;
		GObject *simulationButton;
		GObject *configureButton;
		GObject *quitButton;
		GObject *mainInfoFrameText;
		GtkTextBuffer *textBuffer;
		GtkTextTag *boldTag;
		GtkTextTag *redTag;
		GtkTextTag *undelineTag;

		GError *error = NULL;

		void connectMainGObjects();
		void gtkTextViewCreateTags(GtkTextView *gtkTextView);
};

class VulkanUtilsInterface
{
	public:
		VulkanUtilsInterface(int argc, char *argv[]);
		~VulkanUtilsInterface();
		void start();
	protected:
	private:
		GtkBuilder *mainBuilder;
		VulkanUtilsInterfaceMainWindow *vulkanUtilsInterfaceMainWindow;
		GError *error = NULL;

#ifdef NDEBUG
		//const std::vector<const char*> requiredLayersNames = {"VK_LAYER_LUNARG_standard_validation"};
#else
		//static const std::vector<const char*> requiredLayersNames = { };
#endif
};

#endif
