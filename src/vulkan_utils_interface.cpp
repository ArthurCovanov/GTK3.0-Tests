#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>

#include "vulkan_utils_interface.h"

bool newBrutalEngine()
{
	//return FALSE;
	return TRUE;
}

bool initBrutalEngine()
{
	return FALSE;
	//return TRUE;
}

static void print_hello(GtkWidget *widget, gpointer data)
{
	VulkanUtilsInterfaceMainWindow *vulkanUtilsInterfaceMainWindow = (VulkanUtilsInterfaceMainWindow *)data;
	vulkanUtilsInterfaceMainWindow->addTstTxt();
	widget = widget;
	data = data;
	g_print("Hello World\n");
}


VulkanUtilsInterfaceMainWindow::VulkanUtilsInterfaceMainWindow(GtkBuilder *builder) : builderUi("builder.ui")
{
	this->builder = builder;
	if(gtk_builder_add_from_file(this->builder, builderUi, &error) == 0)
	{
		g_printerr("Error loading file: %s\n", error->message);
		g_clear_error (&error);
		//return EXIT_FAILURE;
		//Throw error
	}

	mainWindow = gtk_builder_get_object(this->builder, "main_window");
	launchButton = gtk_builder_get_object(this->builder, "launch_button");
	simulationButton = gtk_builder_get_object(this->builder, "simulation_button");
	configureButton = gtk_builder_get_object(this->builder, "configure_button");
	quitButton = gtk_builder_get_object(this->builder, "quit_button");
	quitButton = gtk_builder_get_object(this->builder, "quit_button");
	mainInfoFrameText = gtk_builder_get_object(this->builder, "main_info_frame_text");
	gtkTextViewCreateTags(GTK_TEXT_VIEW(mainInfoFrameText));
	connectMainGObjects();
}

VulkanUtilsInterfaceMainWindow::~VulkanUtilsInterfaceMainWindow()
{
}

void VulkanUtilsInterfaceMainWindow::gtkTextViewCreateTags(GtkTextView *gtkTextView)
{
	textBuffer = gtk_text_view_get_buffer(gtkTextView);
	boldTag = gtk_text_buffer_create_tag(textBuffer, "bold", "weight", PANGO_WEIGHT_BOLD, NULL);
	redTag = gtk_text_buffer_create_tag(textBuffer, "red", "foreground", "red", NULL);
	undelineTag = gtk_text_buffer_create_tag(textBuffer, "undeline", "underline", PANGO_UNDERLINE_SINGLE, NULL);
}

void VulkanUtilsInterfaceMainWindow::connectMainGObjects()
{
	g_signal_connect(mainWindow, "destroy", G_CALLBACK(gtk_main_quit), NULL);
	g_signal_connect(mainWindow, "activate-default", G_CALLBACK(print_hello), NULL);
	g_signal_connect(launchButton, "clicked", G_CALLBACK(print_hello), NULL);
	g_signal_connect(simulationButton, "clicked", G_CALLBACK(print_hello), NULL);
	g_signal_connect(configureButton, "clicked", G_CALLBACK(print_hello), this);
	g_signal_connect(quitButton, "clicked", G_CALLBACK(gtk_main_quit), NULL);
}

void VulkanUtilsInterfaceMainWindow::addTstTxt()
{
	GtkTextIter iter;
	gtk_text_buffer_get_end_iter(textBuffer, &iter);

	gtk_text_buffer_insert_with_tags(textBuffer, &iter, "Test: ", sizeof("Test: ")-1, boldTag, undelineTag, NULL);
	gtk_text_buffer_insert(textBuffer, &iter, "Ok?\n", sizeof("Ok?\n")-1);
	gtk_text_buffer_insert_with_tags(textBuffer, &iter, "ERROR!!!\n", sizeof("ERROR!!!\n")-1, redTag, NULL);
}

void VulkanUtilsInterfaceMainWindow::start()
{
	gtk_widget_show_all(GTK_WIDGET(mainWindow));

	addTstTxt();

	gtk_main();
}

VulkanUtilsInterface::VulkanUtilsInterface(int argc, char *argv[])
{
	gtk_init(&argc, &argv);
	mainBuilder = gtk_builder_new();
	vulkanUtilsInterfaceMainWindow = new VulkanUtilsInterfaceMainWindow(mainBuilder);
	/* Construct a GtkBuilder instance and load our UI description */
	
}

VulkanUtilsInterface::~VulkanUtilsInterface()
{
	delete(vulkanUtilsInterfaceMainWindow);
}

void VulkanUtilsInterface::start()
{
	vulkanUtilsInterfaceMainWindow->start();
}

int launchInterface(int argc, char *argv[])
{
	/*VulkanUtilsInterface *vulkanUtilsInterface = new VulkanUtilsInterface(argc, argv);
	vulkanUtilsInterface->start();
	delete(vulkanUtilsInterface);*/
	VulkanUtilsInterface vulkanUtilsInterface(argc, argv);
	vulkanUtilsInterface.start();
	return EXIT_SUCCESS;
}

